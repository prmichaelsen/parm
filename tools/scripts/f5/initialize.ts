#!/usr/bin/env ts-node-script
import * as firestoreService from 'firestore-export-import';
import { fetch } from './apps';
import { resolve, restore } from './util';
import * as fs from 'fs';
import * as nunjucks from 'nunjucks';

// run this script by entering this into the terminal: 
// ./tools/f5/backup.ts
const main = async () => {
  const argPositionsByName = {
    2: 'appNames',
    3: 'projectId',
  }
  const [ 
    appNames,
    projectId,
  ] = Object.keys(argPositionsByName)
    .map(key => process.argv[key]);

  const appNamesArr = (appNames + '').split(',');
  const allApps = (await fetch.apps()).map(conf => conf.app);
  const apps = 
    allApps.filter(app => appNamesArr.some(m => m === app));
  if (apps.length === 0) {
    console.log(`not a valid application '${appNames}'`);
    return;
  }

  appNamesArr.forEach(appName => {
    const f5 = resolve(`./apps/f5/`);
    const nodeLibrary = require(f5 + '/templates/initial-nodes.json');
    const nodeLibPath = resolve(f5 + `/artifacts/${appName}.library.json`);
    const file = nunjucks.render(f5 + '/templates/initial-nodes.json.njk', {
      app: appName,
      nodes: JSON.stringify(nodeLibrary['node-library'], null, 2),
    });
    fs.writeFileSync(nodeLibPath, file, 'utf8');

    // Start importing your data
    restore({
      restoreFileJsonPath: nodeLibPath,
      restoreApps: [appName],
      ...(projectId ? {
        databaseUrl: `https://${projectId}.firebaseio.com`,
        serviceAccountJsonPath: `./env/${projectId}.json`,
      } : {}),
    });
  });
}
main();