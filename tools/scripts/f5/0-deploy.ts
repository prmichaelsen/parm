#!/usr/bin/env ts-node-script
import { preBundle } from './pre-bundle';
import { deploy } from './deploy';
import { postBundle } from './post-bundle';
import { bundle } from './bundle';
import { preDeploy } from './pre-deploy';
import { fetch } from './apps';

type ValidStep = 
  'pre-bundle'| 'bundle'| 'post-bundle'| 'pre-deploy'| 'deploy'
;
type ValidSteps = Array<ValidStep>;

const help = 
`
Provide comma delimited app names
to deploy specific applications.
`
const argPositionsByName = {
  2: 'appNames',
  3: 'steps',
  4: 'sequential',
}
const [ 
  appNames,
  steps,
  sequential,
] = Object.keys(argPositionsByName)
  .map(key => process.argv[key]);

const isSequentialDeployment = sequential === 'true';

const main = async () => {
  const appNamesArg: string = appNames + '';
  const appNamesArr = appNames.split(',');
  const appConfigs = await fetch.apps();
  const allApps = appConfigs.map(conf => conf.app);
  let apps = 
    allApps.filter(app => appNamesArr.some(m => m === app));
  if (appNamesArg === '_') {
    apps = allApps;
  } else if (appNamesArg.startsWith('_')) {
    const archetype = appNamesArg.substr(1);
    apps = appConfigs
      .filter(app => app.archetype === archetype)
      .map(conf => conf.app)
    ;
  } else if (apps.length === 0) {
    console.log(`not a valid application '${appNames}'`);
    return;
  } 
  const stepsArr = (steps + '').split(',') as ValidSteps;
  const allSteps: ValidStep[] = [
    'pre-bundle', 'bundle', 'post-bundle', 'pre-deploy', 'deploy'
  ];
  let runSteps = 
    allSteps.filter(app => stepsArr.some(m => m === app));
  if (runSteps.length === 0) {
    runSteps = allSteps;
  }

  const executeStep = async (step, apps) => {
    !sequential ? await step(apps)
      : await Promise.all(
        apps.map(async app => await step([app]))
      );
  }
 
  if (runSteps.some(s => s === 'pre-bundle'))
    await executeStep(preBundle, apps);
  if (runSteps.some(s => s === 'bundle'))
    await executeStep(bundle, apps);
  if (runSteps.some(s => s === 'post-bundle'))
    await executeStep(postBundle, apps);
  if (runSteps.some(s => s === 'pre-deploy'))
    await executeStep(preDeploy, apps);
  if (runSteps.some(s => s === 'deploy'))
    await executeStep(deploy, apps);
};

main();