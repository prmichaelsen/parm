export interface App {
  app: string,
  title: string,
  metaTitle: string,
  metaDescription: string,
  header: string,
  numResponses: number,
  maxResponses: number,
  host: string,
  database?: string;
  archetype?: string,
}