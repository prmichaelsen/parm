#!/usr/bin/env ts-node-script

import { 
  backup,
} from './util';
import { fetch } from './apps';

// run this script by entering this into the terminal: 
// ./tools/f5/backup.ts
const main = async () => {
  const argPositionsByName = {
    2: 'appNames',
    3: 'projectId',
  }
  const [
    appNames,
    projectId,
  ] = Object.keys(argPositionsByName)
    .map(key => process.argv[key]);

  const appNamesArr = (appNames + '').split(',');
  const allApps = (await fetch.apps()).map(conf => conf.app);
  const apps =
    allApps.filter(app => appNamesArr.some(m => m === app));
  if (
      appNames !== undefined
      && apps.length === 0 
      && appNamesArr.length !== 0
    ) {
    console.log(`[ERROR]: Not a valid application '${appNames}'`);
    return;
  }
  const backupApps = apps.length ? apps : allApps;

  await backup({ 
    backupApps,
    ...(projectId ? {
      databaseUrl: `https://${projectId}.firebaseio.com`,
      serviceAccountJsonPath: `./env/${projectId}.json`,
    } : {}),
  });
}

main();