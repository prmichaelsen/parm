import DeepDiff from 'deep-diff';
import { Migration } from '../migrate';

console.info('[INFO] Migration module require succeeded.');
export const migration: Migration = function (database: {}) {
  const databaseNew = {};
  Object.keys(database)
    .filter(collectionName => /\w+/.test(collectionName))
    .forEach(collectionName => {
      const collection = { ...database[collectionName] };
      databaseNew[collectionName] = collection;
      Object.keys(collection).forEach(nodeId => {
        const node = { ...collection[nodeId] };
        const text: string = node.text || '';
        const regex = /\/?\?focus=/g;
        const textNew = 
          text
            .replace(regex, '/nodes/')
            .replace(/\<ImgUploader\/\>/, '\<ImgUploaderPlus/>')
          ;
        node.text = textNew;
        collection[nodeId] = node;
      });
    });
  return {
    databaseNew,
    database,
    diff: DeepDiff(database, databaseNew),
  };
};