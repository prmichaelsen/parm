#!/usr/bin/env ts-node-script
import { exit } from 'process';
import { fetch } from './apps';
import { backup, resolve, restore, writeJson } from './util';
import path from 'path';
const readdirsr =  require('recursive-readdir');

const migrationsDir = './tools/scripts/f5/migrations';

export type Migration =
  (database: {}) => ({
    database: {},
    databaseNew: {},
    diff: {},
  });

export type MigrationFactory =
  (params: {
    filename: string,
    migration: Migration
    appNames: string[],
  }) => ({
    /** preview migration diff */
    preview: (database: any) => Promise<number>,
    /** apply migration */
    apply: (database: any) => Promise<number>,
  });

const migrationFactory: MigrationFactory =
  function ({
    filename, migration, appNames
  }) {

    const name = path.parse(filename).name;
    const strategy = {

      preview: async (database: any) => {
        const { diff, databaseNew  } = migration(database);
        const databaseNewJsonPath = `./tmp/${name}-migration-diff.json`;
        await writeJson({
          fp: databaseNewJsonPath,
          data: diff,
        });
        const migrationsJsonPath = `./tmp/${name}-migration.json`;
        await writeJson({
          fp: migrationsJsonPath,
          data: databaseNew,
        });
        console.info(`[INFO]: migration diff written to ${databaseNewJsonPath}`);
        console.info(`[INFO]: migration preview written to ${migrationsJsonPath}`);
        return 0;
      },

      apply: async (database: any) => {
        const databaseNewJsonPath = `tmp/${name}-migration.json`;
        await restore({
          restoreApps: appNames,
          restoreFileJsonPath: databaseNewJsonPath,
        });
        const migrationsJsonPath = 'tmp/migrations.json';
        writeJson({
          fp: migrationsJsonPath,
          data: { ...appNames.map(appName => ({ [appName]: { [name]: true }}))},
        });
        await restore({
          restoreApps: appNames,
          restoreFileJsonPath: migrationsJsonPath,
        });
        return 0;
      },
    }
    return strategy;
  }


{
  type ValidStep =
    'preview' | 'apply';
  ;

  const help = `args:
  command:
    preview - preview the migration
    apply - apply the migration
  flags:
    flag args are position. to skip an arg,
    pass _ _
    --migrations - comma delimited list
      of specific migrations to apply
    --apps - comma delimited list of
      specific apps to migrate
`;
  const argPositionsByName = {
    2: 'command',
    3: '--migrations',
    4: 'migrations',
    5: '--apps',
    6: 'appNames',
  }


  const main = async () => {
    const args = Object.keys(argPositionsByName)
      .map(key => process.argv[key]);
    const [
      command,
      migrationsFlag = '_',
      migrations = '',
      appsFlag = '_',
      appNames = '',
    ] = args;
    
    let backupApps = (await fetch.apps()).map(conf => conf.app);
    if (appsFlag !== '_') {
      const appNamesArr = appNames.split(',');
      const apps =
        backupApps.filter(app => appNamesArr.some(m => m === app));
      if (apps.length === 0 && appNames !== undefined) {
        console.error(`[ERROR]: Not a valid application '${appNames}'`);
        exit(127);
      } 
      backupApps = apps.length ? apps : backupApps;
    }

    const fileNames: string[] = await new Promise((res, rej) =>
      readdirsr(migrationsDir, (err, files) => {
        if (err)
          rej(err);
        res(files);
      })
    );
    const migrationDatePattern = /\d{4}-\d{2}-\d{2}\.ts/
    const migrationsArr = fileNames
      .filter(m => migrationDatePattern.test(m))
      .sort((a, b) => {
        const _a = new Date(a);
        const _b = new Date(b);
        return +_a - +_b;
      })
    ;
    const migrationTargets = [];
    const migrationsArgs = migrations.split(',');
    if (migrationsFlag !== '_' && migrations.length > 0) {
      migrationTargets.push(...migrationsArgs);
    } else {
      migrationTargets.push(...migrationsArr);
    }
    const migrationFilter = t => migrationsArr.some(
      m => path.parse(m).name === path.parse(t).name
    );

    const invalidMigrationsTargets =
      migrationTargets.filter(
        t => !migrationFilter(t)
      );
    if (invalidMigrationsTargets.length > 0) {
      const badTargets = invalidMigrationsTargets.join('\n');
      console.error(`[ERROR] Could not find migration files for:
${badTargets}
`);
      console.info(help);
      exit(127);
    }
    const validMigrationTargets = 
      migrationTargets.filter(migrationFilter);

    await Promise.all(validMigrationTargets.map(async migrationFilename => {
      const migrationPath = resolve(`${migrationsDir}/${path.parse(migrationFilename).base}`);
      const migrationSpec: Migration = require(migrationPath).migration;
      const migration = migrationFactory({
        filename: `${migrationFilename}`,
        migration: migrationSpec,
        appNames: backupApps,
      });
      const backupFileJsonPath = `tmp/${path.parse(migrationFilename).name}-migration-rollback.json`;
      // this works in backup.ts but not in migrate?
      await backup({ 
        backupApps,  
        backupFileJsonPath,
      });
      console.info(`[INFO]: migration rollback written to ${backupFileJsonPath}`);
      const database = require(resolve(backupFileJsonPath));
      let code = 0;
      switch (command as ValidStep) {
        case 'preview':
          code = await migration['preview'](database);
          break;
        case 'apply':
          code = await migration['preview'](database);
          code = await migration['apply'](database);
          break;
        default: {
          console.error(`[ERROR] Missing argument 'command' at position '2'`);
          console.info(help);
          exit(127);
        }
      }
      if (code !== 0) {
        console.error(`[ERROR] Error executing '${command}' for migration ${migrationFilename}.`);
        exit(code);
      }
    }));
    exit(0);
  }
  main();
}