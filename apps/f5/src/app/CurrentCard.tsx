import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useQueryParams, StringParam } from 'use-query-params';
import { AdventureOptionCard } from './AdventureOptionCard';
import { useData } from './firebase';
import { NodeEntity } from './Node/NodeDefinition';
import { useStyles } from './useStyles';

export const CurrentCard = () => {
  const classes = useStyles();
  const { 
    state: data, setCurrent: setCurrentState,
    createOption, updateNode,
  } = useData();
  const match = useRouteMatch('/nodes/:id');
  const focus = match && match.params['id'] || 'create';
  const nodes: NodeEntity[] = data.nodes;
  console.log(nodes);
  const focusNode = nodes.find(n => n.id === focus);
  return (
    <div className={classes.cards}>
      {focusNode && (
        <AdventureOptionCard
          createOption={updateNode}
          key={'focus'}
          {...focusNode}
          prev
        />
      )}
    </div>
  );
}