import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useQueryParams, StringParam } from 'use-query-params';
import { AdventureOptionCard } from './AdventureOptionCard';
import { useData } from './firebase';
import { useStyles } from './useStyles';
import { NodeEntity } from './Node/NodeDefinition';

export const CreateCard = () => {
  const classes = useStyles();
  const { 
    state: data, setCurrent: setCurrentState,
    createOption, updateNode,
  } = useData();
  const match = useRouteMatch('/nodes/:id');
  const focus = match && match.params['id'] || 'create';
  const rootId = data.root && data.root.id;
  return (
    <div className={classes.cards}>
      {focus === 'create' && (
        <AdventureOptionCard
          showBackButton={false}
          key={'add'}
          new
          parent={rootId}
          createOption={createOption}
          type={'action'}
        />
      )}
    </div>
  );
}