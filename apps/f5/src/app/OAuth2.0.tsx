import React, { useState, useCallback, useEffect } from 'react';
import uuidv1 from 'uuid/v1';
import { reddit } from '@parm/util';
import { StringParam, useQueryParams } from 'use-query-params';
import Axios from 'axios';
import Button from '@material-ui/core/Button';
import { RedditTokenManager } from './storage';
import { useHistory } from 'react-router-dom';

export const OAuth20 = () => { 
  const [uuid] = useState(uuidv1());
  /**
   * all uris redirect to parm.app.
   * this url param instructs the webapp
   * to redirect to the correct host
   * for completing the authorizations
   * process
   */
  const origin = window.location.origin;
  const [token, setToken] = useState(null);
  const [{
    code, state,
    refreshToken, accessToken,
  }, _] = useQueryParams({
    code: StringParam,
    state: StringParam,
    accessToken: StringParam,
    refreshToken: StringParam,
  });
  const sourceOrigin = state;
  const redditAppName = reddit.clientId;
  const baseUrl = 'https://www.reddit.com/api/v1/authorize?';
  const redirectUri = 'https://parm.app/nodes/authorize-reddit';
  const parts = [
    `client_id=${redditAppName}`,
    `response_type=code`,
    `duration=permanent`,
    `state=${window.location.origin}`,
    `redirect_uri=${redirectUri}`,
    `scope=${[
      'edit',
      'submit',
      'read',
      'vote',
    ].join(' ')}`
  ];

  const link = `${baseUrl}${parts.join('&')}`;

  useEffect(() => {
    if (accessToken && refreshToken) {
      const token = {
        accessToken,
        refreshToken,
      }
      RedditTokenManager.set(token);
      setToken(token);
    }
  }, [accessToken, refreshToken]);

  const onCompleteAuthClick = useCallback(async () => {
    const encode = window.btoa(`${reddit.clientId}:${reddit.clientSecret}`);
    const response = await Axios.post<{
      access_token: string;
      refresh_token: string;
      state: string,
    }>(
      'https://www.reddit.com/api/v1/access_token',
      null,
      {
        headers: {
          "Authorization": `Basic ${encode}`,
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        params: {
          grant_type: 'authorization_code',
          code,
          redirect_uri: redirectUri,
        }
      });
    const token = {
      accessToken: response.data.access_token,
      refreshToken: response.data.refresh_token,
    }
    const sourceHost = state;
    if (
      window.location.origin !== sourceHost
      && token.accessToken
      && token.refreshToken
    ) {
      const params = new URLSearchParams();
      params.set('accessToken', token.accessToken);
      params.set('refreshToken', token.refreshToken);
      params.set('state', state);
      const query = params.toString();
      window.location.href = [
        state,
        `/nodes/reddit-authorization`,
        `?${query}`
      ].join('');
    }
    RedditTokenManager.set(token);
    setToken(token);
  }, [code]);

  const hasToken = token && token.refreshToken !== undefined
    && token.accessToken !== undefined;

  return (
    <>
    {hasToken && (
      <div>
        Congratulations! You have successfully authorized parm with reddit.
      </div>
    ) ||
    code && (
      <div>
        <Button onClick={onCompleteAuthClick}>
          Complete Authorization
        </Button>
      </div>
    ) || (
      <div>
        <Button href={link} target="_blank">
          Authorize Reddit
        </Button>
      </div>
    )}
    </>
  );
}

