// TODO extract into 3rd party
// lib
export type NodeEntityType =
| 'prompt'
| 'action'
| 'redirect'
| 'image'
;

// TODO extract into 3rd party
// lib
export interface NodeEntity {
  parent: string;
  children: string[];
  text: string;
  id: string;
  type: NodeEntityType;
  creatorId: string;
  createTime: firebase.firestore.Timestamp;
  updateTime: firebase.firestore.Timestamp;
  isRoot?: boolean;
  data?: {[key: string]: any }
}

export interface NodeBaseProps {
  text: string,
  parent: string,
  type: NodeEntityType,
}

export interface NodeUpdateProps {
  id: string,
  text?: string,
  data: any,
}

export type NodeCreateProps = NodeBaseProps & Partial<NodeEntity>;