import { sendgrid as secrets } from '@parm/util';
import sendgrid from '@sendgrid/mail';
import { Request, Response } from 'express';

export interface SendEmailRequest {
  to: string,
  from: string,
  subject: string,
  text: string,
  html: string,
}

export const functionSendEmail = async (
  req: Request<SendEmailRequest>, 
  res?: Response
) => {
  try {
    res.set('Access-Control-Allow-Origin', 'https://ricksfeed.app');
    // res.set('Access-Control-Allow-Origin', 'http://localhost:4203');
    res.set('Access-Control-Allow-Credentials', 'true');
  
    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Headers', 'access-control-allow-origin,content-type');
      res.set('Access-Control-Allow-Methods', 'GET,POST');
      res.set('Access-Control-Max-Age', '3600');
      return res.status(204).send({});
    }

    const {
      to, from, subject, text, html
    } = req.body;
    sendgrid.setApiKey(secrets.apiKey);
    const msg = {
      to, from, subject, text, html
    };
    // using SendGrid's v3 Node.js Library
    // https://github.com/sendgrid/sendgrid-nodejs
    const [clientRes, data] = await sendgrid.send(msg);
    if (
      clientRes.statusCode === 200
      || clientRes.statusCode === 202
    ) {
      return res.status(200).send({
        code: 200,
        data: {},
        message: 'Email queued successfully.',
      });
    } else {
      throw { 
        code: clientRes.statusCode,
        data,
        message: clientRes.body,
      };
    }
  } catch (e) {
    const message = e && e.message || 'Unknown error occurred.';
    const data = e.data || {};
    console.error(`[ERROR]: ${message}`, e);
    return res.status(e.code || 500).send(
      { message, data }
    );
  }
};

/** test event
{       
  "to": "patrickazusa@gmail.com",
  "from": "rick@ricksfeed.app",
  "subject": "test",
  "text": "hello world",
  "body": "hello world"
}
*/